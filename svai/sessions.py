import subprocess
import time
import socket
from threading import Thread, Event, Lock
import asyncio
import logging
import psutil
from queue import Queue

from mono_injector import MonoInjector
from solo_data import ClassData, BotLevel

class Session:
    def __init__(self, process, conn):
        super().__init__()
        self.process = process
        self.sock = conn
        
        self.listener_thread = Thread(target=self._listener, name='PID {} Listener'.format(self.process.pid))
        self.listener_thread.setDaemon(True)
        self.kill_switch = Event()
        self.online = False
        self.game_started = False
        self.lock = Lock()

    def _listener(self):
            while not self.kill_switch.is_set:
                pass

    def reset(self, online=False, **kwargs):
        """
            Parameters: 
                * online | boolean | match online?(default: False)
                * opponent_class | ClassData | opponent class(default: random)
                * opponent_level | BotLevel | opponent level(default: random)
                * player_class | ClassData | player class(default: Runecraft)
                * player_deck | list of int | List of card id(default: empty)
            Returns: observation
        """
        kwargs.setdefault('opponent_class', ClassData.random())
        kwargs.setdefault('opponent_level', BotLevel.random())
        kwargs.setdefault('player_class', ClassData.Runecraft)
        kwargs.setdefault('player_deck', [])

        self.online = online

        if not kwargs['player_deck']:
            raise ValueError('You must specify player_deck!')
        # start listener thread
        self.listener_thread.start()
        # send pid to the instance

        #offline mode
        if not online:
            # send StartAIBattle to agent
            # wait until battle start (OK from agent)
            self.game_started = True

    def step(self, actions):
        """
            Parameters: actions
            Returns: observation, reward, game_state
        """
        # check if the game still playing
        # do whatever you need to do
        # wait until turn start

    def _sync(self):
        """
            Sync client states
        """

class SessionManager:
    def __init__(self, addr=('127.0.0.1', 0xface), asm=open('svai/lib/SVSpy.dll', 'rb').read(), exe_path='modified_game/Shadowverse.exe'):
        super().__init__()
        self.exe_path = exe_path
        self.asm = asm
        self.sessions = []

        # make a TCP server for listen connections
        self.server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_sock.settimeout(3)
        self.server_sock.bind(addr)
        self.kill_switch = Event()

    def _server_main(self, env_n, connections):
        while not self.kill_switch.is_set():
            self.server_sock.listen(env_n)
            try: 
                # accept connections
                conn, addr = self.server_sock.accept()
                logging.info('Accrpt connection from {}'.format(addr[1]))
                connections.append(conn) # only store connection and port
            except socket.timeout:
                continue
        logging.info("Server thread dead")


    # def spawn(self):
    #    pid = subprocess.Popen(self.exe_path, close_fds=True).pid
    #    # Wait for few second before all game initialized
    #    time.sleep(3)
    #    # Inject SVSpy
    #    logging.info('Injecting SVSpy...')
    #    injector = MonoInjector(pid)
    #    injector.inject(self.asm, 'SVSpy', 'Loader', 'Load')
    #    time.sleep(2)
    #    psutil.Process(pid)

    def spawn(self, env_n=1):
        connections = []
        self.kill_switch.clear()
        # Spawn server thread
        self.server_thread = Thread(target=self._server_main, args=[env_n, connections])
        self.server_thread.setDaemon(True)
        self.server_thread.start()
        # Launch env_n games
        logging.info('Spawning {} sessions...'.format(env_n))
        pids = []
        for i in range(env_n):
            pids.append(subprocess.Popen(self.exe_path, close_fds=True).pid)
        # Wait for few second before all game initialized
        time.sleep(3)
        # Inject SVSpy
        logging.info('Injecting SVSpy...')
        for pid in pids:
            injector = MonoInjector(pid)
            injector.inject(self.asm, 'SVSpy', 'Loader', 'Load')

        time.sleep(2)
        # Pair connection with pid
        logging.info('Wait for initialize...')
        for pid in pids:
            ## check see what the process listen to
            process = psutil.Process(pid)
            for listen_info in process.connections():
                for conn in connections:
                    if conn.getpeername()[1] == listen_info.laddr.port:
                        self.sessions.append(Session(process, conn))
        session_found = len(self.sessions)
        logging.info('{} sessions found'.format(session_found))
        if session_found < env_n:
            logging.error('Missing some of the sessions!')
            exit(-1)
            ## if it pair, create session
        self.kill_switch.set()
    
    def despawn(self):
        logging.debug('Despawning sessions...')
        for session in self.sessions:
            session.process.kill()
        self.sessions.clear()
