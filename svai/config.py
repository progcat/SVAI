import argparse

def make_config():
    parser = argparse.ArgumentParser()
    # hyperparameters
    parser.add_argument('--total_card_num', type=int, default=2839, 
                        help='Total number of cards available in game.')
    parser.add_argument('--discount_factor', type=float, default=0.99, 
                        help='Factor for calculate discounted reward(or returns)')
    parser.add_argument('--ppo_epsilon', type=float, default=0.2, 
                        help='Variable for clamping the Loss')
    parser.add_argument('--core_dim', type=int, default=128, help='')
    parser.add_argument('--cardenc_out_dim', type=int, default=30, help='Output dimension of Card Encoder')
    parser.add_argument('--fieldenc_out_dim', type=int, default=30, help='Output dimension of Follower Encoder')
    # parser.add_argument('', type=, default=, help='')
