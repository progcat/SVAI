from svai.models import BabyActor, BabyCritic

class Worker:
    def __init__(self, id):
        super().__init__()
        self.actor = BabyActor()
        self.critic = BabyCritic()

    def train(self, net_params):
        pass

    def _rollout(self):
        pass

    def _learn(self):
        pass