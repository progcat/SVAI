# This model is for bootstrapping the project
import torch
import torch.nn as nn
from torch.distributions import Categorical

# Apply random policy on card choosing
# * No deck prediction
# * No Mulligan
# * No Fusion

class CardEncoder(nn.Module):
    def __init__(self, config):
        super(CardEncoder, self).__init__()
        self.out_dim = config.cardenc_out_dim
        self.card_embed = nn.Embedding(config.total_card_num, 200)
        self.card_embed_out_dim = self.card_embed.embedding_dim * config.total_card_num
        self.fc = nn.Sequential(
            nn.BatchNorm1d(self.card_embed_out_dim + 26), # 200 + 26 = 226
            nn.Linear(self.card_embed_out_dim + 26, 100),
            nn.LeakyReLU(),
            nn.Linear(100, self.out_dim),
            nn.LeakyReLU(),
        )

    def forward(self, card_id, card_stats):
        x = torch.cat([self.card_embed(card_id), card_stats], 1)
        return self.fc(x)

class FieldEncoder(nn.Module):
    def __init__(self, config):
        super(FieldEncoder, self).__init__()
        self.out_dim = config.fieldenc_out_dim
        self.card_embed = nn.Embedding(config.total_card_num, 200)
        self.card_embed_out_dim = self.card_embed.embedding_dim * config.total_card_num
        self.fc = nn.Sequential(
            nn.BatchNorm1d(self.card_embed_out_dim + 29), # 200 + 29 = 229
            nn.Linear(self.card_embed_out_dim + 29, 100),
            nn.LeakyReLU(),
            nn.Linear(100, self.out_dim),
            nn.LeakyReLU(),
        )

    def forward(self, card_id, follower_stat):
        x = torch.cat([self.card_embed(card_id), follower_stat], 1)
        return self.fc(x)

class Observation(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.players_info_layer = nn.Sequential(
            # two player info plus 2 game info
            nn.Linear(27 * 2 + 2, 20), # 56
            nn.LeakyReLU(),
            nn.Linear(20, 5),
            nn.LeakyReLU(),
        )

        self.card_encoder = CardEncoder(config)
        self.player_hand_maxpool = nn.MaxPool1d(self.card_encoder.out_dim) # must be same as the card encoder output

        self.field_encoder = FieldEncoder(config)
        self.field_maxpool = nn.MaxPool1d(self.field_encoder.out_dim) # must be same as the field encoder output

        input_dim = self.field_encoder.out_dim * 2 + self.card_encoder.out_dim + 5
        self.core = nn.GRU(input_dim, config.core_dim, dropout=0.5)
        
    def brainwash(self):
        self.h_t = torch.zeros(1)
        self.c_t = torch.zeros(1)

    def forward(self, player_info, player_hand, player_field, opponent_info, opponent_field, hidden_state):
        player_hand_maxpooled = self.player_hand_maxpool(torch.Tensor([self.card_encoder(card.id, card.stat) for card in player_hand]))
        player_field_maxpooled = self.field_maxpool(torch.Tensor([self.field_encoder(card) for card in player_field]))
        opponent_field_maxpooled = self.field_maxpool(torch.Tensor([self.field_encoder(card) for card in opponent_field]))

        players_info = self.players_info_layer(torch.cat([torch.Tensor(player_info), torch.Tensor(opponent_info)], 1))

        x = nn.cat([player_hand_maxpooled,
                    player_field_maxpooled,
                    opponent_field_maxpooled,
                    players_info,
                    ], 1)
        
        _, (self.h_t, self.c_t) = self.core(x, self.h_t, self.c_t)

        return self.h_t

class ConfidenceEstimator(nn.Module):
    def __init__(self, input_dim):
        super(ConfidenceEstimator, self).__init__()
        self.win_rate_estimator = nn.Sequential(nn.Linear(input_dim, 1), nn.Sigmoid())

    def forward(self, core_state):
        return self.win_rate_estimator(core_state)

class BabyActor(nn.Module):
    def __init__(self, config):
        super().__init__()

        # see ActionSpace.txt
        self.action_sel_fc = nn.Linear(config.core_dim, 8)
        self.available_actions_embedd = nn.Embedding(8, 2)
        
        self.hand_card_select_fc = nn.Linear(config.core_dim, 9)
        self.affortable_cards_embedd = CardEncoder(config.total_card_num)

        self.player_target_sel = nn.Linear(config.core_dim, 5)
        self.opponent_target_sel = nn.Linear(config.core_dim, 5)

    def selection(self, options, core_oup, embed_layer, fc_layer):
        return nn.Softmax(dim=1)(torch.dot(embed_layer(options), fc_layer(core_oup)))
    
    def forward(self, core_oup, available_actions):
        action_probs = self.selection(available_actions, core_oup, self.available_actions_embedd, self.action_sel_fc)
        aciton_dist = Categorical(action_probs)
        action = aciton_dist.sample()

class BabyCritic(nn.Module):
    def __init__(self, config):
        super(BabyCritic, self).__init__()
        self.state_value_net = nn.Sequential(
            nn.Linear(config.core_dim, 500),
            nn.LeakyReLU(),
            nn.Linear(500, 300),
            nn.LeakyReLU(),
            nn.Linear(300, 1)
        )

    def forward(self, core_state):
        return self.state_value_net(core_state)
        