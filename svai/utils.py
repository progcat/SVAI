import logging
import coloredlogs

def init_logging(log_lv):
    field_style = coloredlogs.DEFAULT_FIELD_STYLES
    field_style['levelname'] = {'color': 'white', 'bold': True}
    
    levels = {'ERROR': logging.ERROR, 'WARN': logging.WARNING, 'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    coloredlogs.install(level=levels[log_lv], 
                        field_styles=field_style, 
                        fmt='%(levelname)5s | %(message)s'
                        )
