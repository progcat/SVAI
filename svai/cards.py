import requests
import sqlite3
import json
def getCardTotal():
    params = {'format': 'json', 'lang': 'en', 'clan': '0'}
    req = requests.get('https://shadowverse-portal.com/api/v1/cards', params=params)
    if req.status_code != 200:
        print('Failed', req.status_code)

    j = json.loads(req.text)

    cards = j['data']['cards']

    card_ids = []
    for card in cards:
        card_id = card["base_card_id"]
        if not card_id in card_ids:
            card_ids.append(card_id)

    return len(card_ids)