import torch.multiprocessing as mp
import torch

from sessions import SessionManager
from utils import init_logging

if __name__ == '__main__':
    init_logging('DEBUG')
    # prepare environment
    session_mgr = SessionManager()
    ## launch multiple sessions
    session_mgr.spawn(env_n=3, asm=open('SVAgent/bin/Debug/SVAgent.dll', 'rb').read())

    # make/load model

    # deploy workers
    

    # Close all sessions
    session_mgr.despawn()