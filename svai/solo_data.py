from enum import IntEnum, unique
import random

# SV version: 3.0.20

@unique
class ClassData(IntEnum):
    Forestcraft = 0,
    Swordcraft = 1,
    Runecraft = 2,
    Dragoncraft = 3,
    Shadowcraft = 4,
    Bloodcraft = 5,
    Havencraft = 6,
    Portalcraft = 7

    def random(self):
        return random.choice(list(self))

@unique
class BotLevel(IntEnum):
    Beginner = 0,
    Experienced = 2,
    Expert = 3,
    Expert2 = 5,
    Elite = 4,
    Elite2 = 6,
    Elite3 = 7

    def random(self):
        return random.choice(list(self))
