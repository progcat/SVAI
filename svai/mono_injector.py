import clr
import os

class MonoInjector:
    def __init__(self, pid, lib_path=os.getcwd() + '\svai\lib\SharpMonoInjector.dll'):
        clr.AddReference(lib_path)
        from SharpMonoInjector import Injector as MonoInjector
        self.injector = MonoInjector(pid)

    def inject(self, asm, namespace, classname, funcname):
        self.asm = self.injector.Inject(asm, namespace, classname, funcname)

    def eject(self, namespace, classname, funcname):
        if not self.asm:
            return
        self.injector.Eject(self.asm, namespace, classname, funcname)