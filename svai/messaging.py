from enum import IntEnum, unique

@unique
class MessageType(IntEnum):
    # Status Code
    Ok = 1,
    Error = 2,
    # Requests (Session-to-SV)
    Sync = 3, # Sync all states (info of battle) to Session
    Unload = 4, # Unload SVSpy from the game
    StartAIBattle = 5, # Start a AI battle
    TurnEnd = 6, # End this turn
    PlayCardToField = 7,
    PlayCardToFollower = 8,
    PlayCardToPlayer = 9,
    PlayCardToOpponent = 10,
    AttackFollower = 11,
    AttackPlayer = 12,
    Evolve = 13, # Evolve a follower
    Select = 14, # Select a card, pair with OnChoose

    # Events (SV-to-Session)
    OnBattleStart = 15,
    OnMulliganStart = 16,
    OnTurnStart = 17,
    OnBattleEnd = 18,
    OnChoose = 19,
    OnError = 20, # Error message from SV
    OnInfo = 21 # Info message from SV
    