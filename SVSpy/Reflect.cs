﻿using System;
using System.Reflection;

namespace SVSpy
{
    public static class Reflect
    {
        private static BindingFlags flags = BindingFlags.Instance
                                     | BindingFlags.Public 
                                     | BindingFlags.NonPublic
                                     | BindingFlags.Static
                                     | BindingFlags.FlattenHierarchy;
        // SRC: https://stackoverflow.com/questions/3303126/how-to-get-the-value-of-private-field-in-c
        public static T GetField<T>(this object inst, string field)
        {
            return (T) inst.GetType().GetField(field, flags).GetValue(inst);
        }
        
        public static void SetField<T>(this object inst, string field, object val)
        {
            inst.GetType().GetField(field, flags).SetValue(inst.GetType(), val);
        }

        public static void InvokeFunc(this object inst, string func)
        {
            inst.GetType().GetMethod(func, flags).Invoke(inst, null);
        }
    }
}