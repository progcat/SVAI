﻿using System.Collections.Generic;
using Wizard;

namespace SVSpy
{
    public class SoloData
    {
         public static readonly List<BotData> bot_data = new List<BotData>
         {
             new BotData(1001, "精靈Bot", 1, 1),
             new BotData(2001, "皇家護衛Bot", 2, 4),
             new BotData(3001, "巫師Bot", 3, 7),
             new BotData(4001, "龍族Bot", 4, 3),
             new BotData(5001, "死靈法師Bot", 5, 6),
             new BotData(6001, "吸血鬼Bot", 6, 2),
             new BotData(7001, "主教Bot", 7, 5),
             new BotData(8001, "復仇者Bot", 8, 18)
         };

         public enum DeckLevel
         {
             Beginner = 0,
             Experienced = 2,
             Expert = 3,
             Expert2 = 5,
             Elite = 4,
             Elite2 = 6,
             Elite3 = 7
         }

         
    }

    public class BotData
    {
        private string _name;
        private int _id, _classid, _charid, _fid;
        
        private enum Degree
        {
            Beginner = 400001,
            Experienced = 400002,
            Expert = 400003,
            Elite = 400004
        }
        
        public BotData(int id, string name, int classId, int fId)
        {
            this._id = id;
            this._name = name;
            this._classid = classId;
            this._charid = classId;
            this._fid = fId;
        }

        public PracticeData AsPracticeData(SoloData.DeckLevel deckLv)
        {
            PracticeData data = null;
            switch (deckLv)
            {
                case SoloData.DeckLevel.Beginner:
                    data = new PracticeData(_id, _name, _classid, _charid, (int)Degree.Beginner, (int)deckLv, 0, 10);
                    break;
                case SoloData.DeckLevel.Experienced:
                    data = new PracticeData(_id, _name, _classid, _charid, (int)Degree.Experienced, (int)deckLv, 2, 20);
                    break;
                case SoloData.DeckLevel.Expert:
                case SoloData.DeckLevel.Expert2:
                    data = new PracticeData(_id, _name, _classid, _charid, (int)Degree.Expert, (int)deckLv, 2, 20);
                    break;
                case SoloData.DeckLevel.Elite:
                case SoloData.DeckLevel.Elite2:
                case SoloData.DeckLevel.Elite3: 
                    data = new PracticeData(_id, _name, _classid, _charid, (int)Degree.Elite, (int)deckLv, 2, 20);
                    break;
            }
            return data;
        }
        
        public string Name => _name;

        public int Id => _id;

        public int Classid => _classid;

        public int Charid => _charid;

        public int Fid => _fid;
        
    }
}