﻿using UnityEngine;

namespace SVSpy
{
    public class Loader
    {
        private static GameObject agent_obj = null;
        public static void Load()
        {
            //prevent duplicate
            if (agent_obj != null)
            {
                Unload();
            }
            //create new GameObject for agent
            agent_obj = new GameObject();
            //add agent into the object
            agent_obj.AddComponent<Agent47>();
            //make sure it wont release during level loading
            GameObject.DontDestroyOnLoad(agent_obj);
        }

        public static void Unload()
        {
            GameObject.Destroy(agent_obj);
        }
    }
}