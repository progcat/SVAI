﻿using System.Collections.Generic;
using Wizard;
using System;

namespace SVSpy
{
    public static class SceneControl
    {
        public static void StartAIBattle(List<int> player_deck, int player_class, int enemyClass, SoloData.DeckLevel deckLv)
        {
            try
            {
                //Exit battle if we are already in it
                if (UIManager.GetInstance().GetCurrentScene() == UIManager.ViewScene.Battle)
                {
                    GameMgr.GetIns().GetBattleCtrl().BattleRelease();
                }

                ClassSelectionPage.Mode = ClassSelectionPage.eMode.PracticeSelect;
                DataMgr dataMgr = GameMgr.GetIns().GetDataMgr();
                dataMgr.m_BattleType = DataMgr.BattleType.Practice;
                dataMgr.SetPlayerCharaId(player_class);
                dataMgr.SetCurrentDeckData(player_deck);
                dataMgr.Load();
                dataMgr.SetEnemyCharaId(enemyClass);
                PracticeData practiceData = SoloData.bot_data[enemyClass - 1].AsPracticeData(deckLv);
                PracticeAISettingData settingData = Data.Master.PracticeAISettingList.GetSettingData(enemyClass, practiceData.AIDeckLevel);
                Data.Master.LoadAICsv(new AICsvLoadingInfo(settingData.DeckId, settingData.StyleId, settingData.EmoteId), (System.Action)(() =>
               {
                   UIManager.GetInstance().closeInSceneCenterLoading(true, false);
                   dataMgr.SetCurrentEnemyDeckDataFromAIDeck(settingData.ClassId, settingData.Difficulty, settingData.LogicLevel, settingData.MaxLife, settingData.DeckId, settingData.StyleId, settingData.EmoteId, true);
                   dataMgr.LoadEnemyClassData();
                   dataMgr.PracticeDifficultyDegreeId = practiceData.DegreeId;
                   dataMgr.SetSoroPlay3DFieldID(practiceData.Battle3dFieldId);
                   GameMgr.GetIns().GetDataMgr().Practice3DfieldId = practiceData.Battle3dFieldId;
                   UIManager.GetInstance().ChangeViewScene(UIManager.ViewScene.Battle);
               }));
            }
            catch (Exception e)
            {
                UIManager.GetInstance().CreateConfirmationDialog(e.Message);
                throw;
            }
        }
    }
}