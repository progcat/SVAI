using System;
using Wizard;
using System.Collections.Generic;
using LitJson;

namespace SVSpy
{
    public static class Serialization
    {
        public static JsonData DumpDeck(List<BattleCardBase> deck)
        {
            JsonData deck_json = new JsonData();
            foreach (var card in deck)
            {
                deck_json.Add(DumpCard(card));
            }
            return deck_json;
        }

        public static JsonData DumpCard(BattleCardBase card)
        {
            JsonData card_data = new JsonData();
            card_data["name"] = card.BaseParameter.CardName;
            /* Basic info */
            card_data["id"] = card.BaseParameter.CardId;
            card_data["can_play"] = card.AreCanPlayConditionsFulfilled;
            card_data["cost"] = card.Cost;
            card_data["atk"] = card.Atk;
            card_data["life"] = card.Life;
            card_data["max_life"] = card.MaxLife;
            card_data["evo_atk"] = card.BaseParameter.EvoAtk;
            card_data["evo_life"] = card.BaseParameter.EvoLife;
            card_data["evolved"] = card.IsEvolution;
            card_data["can_evolve"] = card.CanEvolution(false, true) | card.CanEvolution(true, true);
            /* Detail Info */
            card_data["atk_count"] = card.AttackableCount;
            // what type can this card transform into
            // card_data["transform_type"] = (int)card.TransformedType;
            card_data["has_spell_charge"] = card.HasSpellCharge;
            card_data["spell_charge_count"] = card.SpellChargeCount;
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 
            // card_data[""] = 


            
            /* Skills */
            // Skills
            // Skills after evolve

            return card_data;
        }

        /* WIP */
        // public static JsonData DumpSkills(SkillCollectionBase skills){
        //     JsonData skill_data = new JsonData();
        //     foreach (var skill in skills)
        //     {
                
        //     }
        //     return skill_data;
        // }

        public static JsonData DumpPlayer(BattlePlayerBase player)
        {
            JsonData player_data = new JsonData();
            player_data["pp"] = player.Pp;
            player_data["pp_total"] = player.PpTotal;
            player_data["life"] = player.Class.Life;
            player_data["max_life"] = player.Class.MaxLife;
            player_data["ep"] = player.CurrentEpCount;
            player_data["max_ep"] = player.EpTotal;
            player_data["can_evo"] = player.IsEvolve;
            player_data["card_played"] = player.TurnPlayCardCount;
            player_data["necro_count"] = player.TurnNecromanceCount;

            return player_data;
        }


    }
}