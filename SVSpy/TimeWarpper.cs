using System;
using UnityEngine;

namespace SVSpy
{
    public static class TimeWarpper
    {
        public static float game_speed { get => Time.timeScale; set => Time.timeScale = value; }
    }
}