﻿using System;
using Cute;
using LitJson;
using SvaiProto;
using UnityEngine;
using Wizard;
using Wizard.Battle.Phase;
using Wizard.Battle.View.Vfx;

// TODO: Maybe I should subsribe to all events, so that we can store gameplays on python side
// TODO: Wizard.Battle.UI.BattleLogManager can provide us battle log, I can return it once battle ended so that I dont need to trace everything
namespace SVSpy
{
    public class Agent47 : MonoBehaviour
    {
        private GUIStyle _label_style = new GUIStyle(){
            fontSize = 15,
            normal = {
                textColor = Color.magenta
            },
        };
        private bool _playing = false;
        public string agent_id;

        private void Start()
        {
            Messenger.Initialize();
            Messenger.OnRecv += OnMessageRecv;
            // Tell python side we are ready
            Messenger.SendMsg(new Status() { });
        }

        private void OnMessageRecv(byte[] buff)
        {
            
        }

        private void LateUpdate()
        {
            Messenger.RecvUpdate();
            BattleMgrBase battle_mgr = ToolboxGame.GameManager.GetField<BattleMgrBase>("_battleMgr");
            if (battle_mgr == null)
            {
                _playing = false;
                return;
            }

            // update states
            IPhase phase = battle_mgr.GetCurrentPhase();
            if (phase is MulliganPhaseBase)
            {
                battle_mgr.MulliganSubmit();
            }
            else if (phase is ResultPhase)
            {
                if (battle_mgr.IsBattleEnd)
                {
                    bool won = !battle_mgr.BattlePlayer.Class.IsDead && battle_mgr.BattleEnemy.Class.IsDead;
                    OnBattleEnd(won);
                    _playing = false;
                }

            }
            else if (phase is LoadingPhase && !_playing)
            {
                SetupEvent(battle_mgr);
                _playing = true;
            }

        }

        private void OnGUI()
        {
            // Unload button
            if(GUI.Button(new Rect(Screen.width - 50, 5, 50, 50), "Unload")){
                Loader.Unload();
            }

            GUI.Label(new Rect(5, 0, 100, 15), "Agent Injected", _label_style);

            UIManager ui = UIManager.GetInstance(); 
            UIManager.ViewScene scene = ui.GetCurrentScene();
            SceneType scene_type = Toolbox.SceneManager.ActiveSceneType;

            GUI.Label(new Rect(5, 15, 100, 15), $"SceneType: {scene_type}", _label_style);
            GUI.Label(new Rect(5, 30, 100, 15), $"Scene: {scene}", _label_style);

            // all about dialogs
            if(ui.NowOpenDialog){
                //close errors
                if(ui.NowOpenDialog.GetTitleLabelStr() == "Error"){
                    ui.CloseLatestDialog();
                }
                //button for other dialog
                if (GUI.Button(new Rect(5, 45, 100, 30), $"Close {ui.NowOpenDialog.GetTitleLabelStr()}"))
                {
                    // ui.CloseNotLatestDialogAll();
                    ui.CloseLatestDialog();
                }
            }

            GUI.Label(new Rect(5, Screen.height-15, 100, 15), $"Speed: {Time.timeScale}x", _label_style);

            if (ToolboxGame.GameManager != null)
            {
                BattleMgrBase battle_mgr = ToolboxGame.GameManager.GetField<BattleMgrBase>("_battleMgr");
                GUI.Label(new Rect(5, 60, 100, 15), $"Phase: {battle_mgr.GetCurrentPhase()}", _label_style);
                GUI.Label(new Rect(5, 75, 100, 15), $"BattleEnd:{battle_mgr.IsBattleEnd}", _label_style);
            }
        }

        private void OnApplicationQuit()
        {
            Messenger.Close();
        }

        private void SetupEvent(BattleMgrBase battle_mgr)
        {
            // subscribe events if new game had been started
            BattlePlayer player = battle_mgr.BattlePlayer;
            BattleEnemy enemy = battle_mgr.BattleEnemy;

            battle_mgr.OnStartOpening += OnBattleStart;

            // player events
            player.OnTurnEndStart += OnTurnEndStart;
            player.OnTurnStartAfterDraw += OnTurnStartAfterDraw;
            //player.OnAddCemeteryEvent
            player.OnAddDeckEvent += OnAddDeck;
            //player.OnAddHandCardAfterEvent
            //player.OnAfterReturnCardEvent
            //player.OnNecromance
            player.OnSpellPlayEvent += (BattleCardBase card) => { UIManager.GetInstance().CreateConfirmationDialog($"OnSpellplayEvent\n{card.CardId}"); };
            //battle_mgr.BattlePlayer.OnFusion
            //battle_mgr.BattlePlayer.OnPpChange
            //battle_mgr.BattlePlayer.OnShortageDeck
            
            // enemy events


            //// operateMgr events
            OperateMgr operate_mgr = battle_mgr.OperateMgr;
            //TODO: Action player
        }

        private void OnTurnEndStart()
        {
            Messenger.SendMsg(new TurnEnd() { });
        }

        private VfxBase OnTurnStartAfterDraw()
        {
            try
            {
                BattleMgrBase battle_mgr = ToolboxGame.GameManager.GetField<BattleMgrBase>("_battleMgr");
                BattlePlayer player = battle_mgr.BattlePlayer;
                BattleEnemy enemy = battle_mgr.BattleEnemy;

                JsonData data = new JsonData();

                data["turn"] = player.Turn;

                
            }
            catch (Exception e)
            {
                UIManager.GetInstance().CreateConfirmationDialog(e.Message);
                //Messenger.SendMsg(Event);
                throw;
            }

            return null;
        }

        //called when game start
        private void OnBattleStart(bool is_first)
        {
            BattleMgrBase battle_mgr = ToolboxGame.GameManager.GetField<BattleMgrBase>("_battleMgr");
            try
            {
                BattlePlayer player = battle_mgr.BattlePlayer;
                BattleEnemy enemy = battle_mgr.BattleEnemy;

                // player_info["class"] = (int)player.Class.Clan;
                
                
                // Speedhack the game
                TimeWarpper.game_speed = Globals.game_speed_mul;
            }
            catch (Exception e)
            {
                UIManager.GetInstance().CreateConfirmationDialog(e.Message);
                //Messenger.SendMsg(Messenger.MessageType.OnError, e.Message);
                throw;
            }
        }

        private void OnAddDeck(BattleCardBase card)
        {

        }
        private void OnBattleEnd(bool won)
        {
            JsonData json = new JsonData();
            json["won"] = won;
            //Messenger.SendMsg(Messenger.MessageType.OnBattleEnd, json.ToJson());
        }
    }
}