﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using LitJson;
using UnityEngine;
using System.Text;
using Google.Protobuf;
using SvaiProto;

namespace SVSpy
{
    public static class Messenger
    {
        private static TcpClient sock = new TcpClient();
        private static IPEndPoint server_address = new IPEndPoint(IPAddress.Loopback, Globals.session_mgr_port);
        public static Action<byte[]> OnRecv;
        public static bool connected = false;
        public static void Initialize()
        {
            sock.Connect(server_address);
        }

        public static void Close()
        {
            sock.Close();
        }

        public static void SendMsg(IMessage msg)
        {
            NetworkStream stream = sock.GetStream();
            msg.WriteTo(stream);
        }

        public static void RecvUpdate()
        {
            try
            {
                connected = sock.Connected;
                if(!sock.Connected) return;
                NetworkStream stream = sock.GetStream();
                if (!stream.DataAvailable) return;
                byte[] buffer = new byte[sock.ReceiveBufferSize];
                int received = stream.Read(buffer, 0, buffer.Length);
                OnRecv?.Invoke(buffer);
            }
            catch
            {
                throw;
            }
        }
    }
}