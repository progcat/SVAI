# SVAI - Play Shadowverse with Reinforement Learning

Attempt to train an GrandMaster with DPPO, also some non-game-breaking hacks.

## Goal

The goal is become GrandMaster in Shadowverse without using MCTS, and try to make an AI to be able to "sense" what cards should be play.

## Project Architecture

Benefit from hacks I did to the game, we can now launch multiple environment at the same time and speed up battles with Cheat Engine's Speed Hack feature. These hacks allow us to train agent much faster! 

In order to be able to interact with the environment, I wrote ```SVSpy``` and inject it into the game with ```SharpMonoInjector.dll```, thus I can access and control the game directly. Therefore, I also establish a TCP connection to the Python side of SVAI so we can interact and getting information from the game.

Shadowverse are using LitJson library, I think it is easier to reuse LitJson for exchange info between C# and Python than rolling our own protocol.

Due to hardware limitations, I can only keep up 3 environment at the same time without lagging, but the whole project is totally scalable. Run many environment as you can!

![](proj_arch.png)

## Techniques Used

tbh, these techniques are making this project **illegal**.

* Mono DLL Injection
* Modify IL Code
* Bypassing Steam Detection
* Speed Hack

## Observation & Action

### Game Flow

1. Choose deck
2. Mulligan stage
3. Draw card
4. Play card
5. Turn end
6. oponent turn
7. if life = 0 we lose

### Observations

* Cards in Hand
* Player Info (HP, EP, BP)
* Class Info (EarthRite, Shadows, Overflow, Number Of Fairies)
* Deck info (Card remain, Number of card remaining)
* Field info (player followers, enemy followers)

### Action Space

* Select card while Mulligan
* Play card to Field
* Play card to Followers
* Play card to Players
* Attack
* Evolution card
* Fusion card
* TurnEnd

## Limitation

* no mulligan
