# Rewards

Zero sum!

Common sense:

* Higher the health the better


|Item|Reward| Condition |
|--|--|--|
| Win/Lose | 10/-10 | |
| PP Remained | -X per PP |
| Player Heal | X per Effective heal point |
| Follower Heal | X per Effective heal point |
| Opponent Max Health Dropped | X per Dropped |
| Player Health Dropped | -X per Dropped |
| Draw Card | X per Card |
| No Hand Card | -X | == 0|
| Hand Shortage | -X | < 2 |
| Deck Shortage | -X | < 10 |
| Draw when full Hand | -X per Card Drawn | |
| Too many Hand | -X | >=8 |
| Attack Player |  | |
| Attack Follower |
| Killed Opponent Follower | X per Follower |
| Follower Dead |
| Evolve | |
| Class Skill Activated |
| Play Card |
| Empty Opponent Field | X | turn-1 have Followers on field |
| OTK | 7 | Kill Opponent with full health |
| 

## Field Advantage

```
field_advantage = followers_on_field * 0.01 +
                    foreach follower(hp * 0.003 + atk * 0.002 + affective_effect_advantages)
```
