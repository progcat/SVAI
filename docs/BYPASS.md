
## Disable steam

1. find SteamManager
2. delete code inside Awake and Update
3. find BootApp
4. delete startSteamClient

## Bypass Mutex

1. find BootApp
2. delete createMutex

## Disable RedShell

1. find BootSystem
2. delete content inside setupRedShell

## Auto game initialize & delete recovery

1. find TitleUI
2. add the following to onFirstStart
```
	RecoveryRecordManagerBase.DeleteRecoveryFile();
    if (this.IsEnableClickButton())
	{
		base.StartCoroutine(this.cuteCertification(delegate
		{
			NtDataTranslateManager.GetInstance().ShowCygamesStatement(new Action(this.attendSetLanguage), true);
		}));
	}
	
```
3. remove this in cuteCertification
```
while (!Toolbox.BootNetwork.IsDoneGameStartCheck)
{
	yield return 0;
}
```
