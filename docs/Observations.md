# Observations

## Game Info

| Field | Observation | Description |
| - | - | - |
| first_hand | 1 | Are we go first? |
| turn | 1 | Current turn |

## Player Info (2 Players)

| Field | Observation | Description |
| - | - | - |
| class_onehot | 8 | One-hoted character class |
| life | 1 | Player Health |
| max_life | 1 | Max player health |
| pp | 1 | Play Point |
| max_pp | 1 | Max play point |
| can_evo | 1 | Can player evolve unlocked |
| ep | 1 | Evolve Point |
| max_ep | 1 | Max evolve point |
| hard_num | 1 | #Hand Card |
| deck_num | 1 | #Deck Card |
| cemetery_num | 1 | #Cemetery Card |
| card_played | 1 | #Card played in this turn |
| **Class Data** | -- | -- |
| officer_num | 1 | #Officer on field |
| commander_num | 1 | #Commander on field |
| spellboost | 1 | #Spellboost |
| earth_rite | 1 | #Earth Rite card on field |
| overflow | 1 | Is Overflow activated? |
| vengeance | 1 | Is Drain activated? |
| resonance | 1 | Is resonance activated? |


## Player Hand

| Field | Observation | Description |
| - | - | - |
| card_id | Embedding | Card ID |
| type_onehot | 3 | Type of the card |
| can_play | 1 | Can we play this card? |
| cost | 1 | Cost |
| atk | 1 | Attack |
| life | 1 | Life |
| max_life | 1 | Max Life |
| evo_atk | 1 | Attack after evolved |
| evo_life | 1 | Life after evolved |
| **Trait One-hot** | -- | Trait of the card |

## Follower

| Field | Observation | Description |
| - | - | - |
| card_id | Embedding | Card ID |
| type_onehot | 3 | Type of the follower |
| player_atkable | 1 | Can attack player? |
| follower_atkable | 1 | Can attack follower? |
| atk_count | 1 | Attack Count |
| atk | 1 | Attack |
| life | 1 | Life |
| max_life | 1 | Max Life |
| evolved | 1 | Evolved? |
| evo_atk | 1 | Attack after evolved |
| evo_life | 1 | Life after evolved |
| trait_onehot | 8 | Trait of the Follower |

