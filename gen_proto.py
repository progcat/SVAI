import subprocess
import struct
import os
import zipfile
try:
    import wget
    use_wget = True
except:
    import urllib.request
    use_wget = False
    
if struct.calcsize('P') == 8:
    # 64bit
    PROTOC_URL = "https://github.com/protocolbuffers/protobuf/releases/download/v3.13.0/protoc-3.13.0-win64.zip"
else:
    # 32bit
    PROTOC_URL = "https://github.com/protocolbuffers/protobuf/releases/download/v3.13.0/protoc-3.13.0-win32.zip"

PROTOC_DIR = 'protoc/'

PROTO_FILE = 'svai_protocol.proto'

PY_OUT = 'svai'

CS_OUT = 'SVSpy'

if __name__ == '__main__':
    if not os.path.exists(PROTOC_DIR):
        print('Downloading protoc...')
        if use_wget:
            wget.download(PROTOC_URL, out="protoc.zip")
        else:
            urllib.request.urlretrieve(PROTOC_URL, 'protoc.zip')

        with zipfile.ZipFile('protoc.zip', 'r') as fp:
            fp.extractall(PROTOC_DIR)
        os.remove('protoc.zip')
        print()

    print('Compile', PROTO_FILE, '...')
    subprocess.Popen(f'{PROTOC_DIR}/bin/protoc.exe --csharp_out={CS_OUT} --python_out={PY_OUT} {PROTO_FILE}', close_fds=True)